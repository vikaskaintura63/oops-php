<?php

class Circle{

    const PI = 3.148;
    public function area($radius){
        return (pow($radius, 2) * self::PI);
    }
}

$Circle = new Circle;
//echo $Circle->area(5);

echo $Circle::PI;