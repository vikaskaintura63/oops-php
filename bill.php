<?php


class Bill{
//methhod chaining

    public $breakfast = 20;

    public $lunch     =  10;

    public $dinner    =  20;

    public $bill;

    public function __construct()
    {
        $this->bill = 4;
    }



  


    public function breakfast($person){
        $this-> bill += $this->breakfast * $person;
        return $this;
    }

    public function lunch($person){
        $this->bill += $this->lunch * $person;
        return $this;

    }

    public function dinner($person){
        $this->bill += $this->dinner * $person;
        return $this;
    }

    public function __destruct()
    {
      echo   $this->bill ;
    }
}

$bill = new Bill();

 $bill->breakfast(3)->lunch(7)->dinner(9)->bill;


