<?php

final class Book{
    //static declaration
    public static $name =  'akash';

    //bind the name 
    final public static function author(){
        return "the author is:" . self::$name;
    }

    //author fetch the name from the above mentioned function
    public static function getAuthor(){
        echo static::author();
    }
}

class newBook extends Book{
    public static function author(){
        return "the auhot of the new book:" . self::$name;
    }
}

Book::getAuthor();
newBook::getAuthor();